﻿Public Class frmChat
    Private _usuario1 As Usuario1
    Private _usuario2 As Usuario1
    Private _lista As List(Of String) = New List(Of String)
    Private _chat As Chat = New ConcreteChat

    Private Sub frmChat_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _usuario1 = New Usuario1("Juan")
        _usuario2 = New Usuario1("Malena")

        _chat.Register(_usuario1)
        _chat.Register(_usuario2)

        _usuario1.Chat = _chat
        _usuario2.Chat = _chat

    End Sub

    Private Sub Listar()
        ListBox1.DataSource = Nothing
        ListBox1.DataSource = _lista

    End Sub

    Private Sub btnEnviarUsuario1_Click(sender As Object, e As EventArgs) Handles btnEnviarUsuario1.Click
        _usuario1.Enviar(txtUsuario1.Text, _usuario2)

        _lista.Add(_usuario1.Nombre & " to " & _usuario2.Nombre & ": " & txtUsuario1.Text)

        Listar()
    End Sub

    Private Sub btnEnviarUsuario2_Click(sender As Object, e As EventArgs) Handles btnEnviarUsuario2.Click
        _usuario2.Enviar(txtUsuario2.Text, _usuario1)

        _lista.Add(_usuario2.Nombre & " to " & _usuario1.Nombre & ": " & txtUsuario2.Text)

        Listar()
    End Sub
End Class
