﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChat
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.txtUsuario1 = New System.Windows.Forms.TextBox()
        Me.btnEnviarUsuario1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtUsuario2 = New System.Windows.Forms.TextBox()
        Me.btnEnviarUsuario2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(13, 144)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(648, 95)
        Me.ListBox1.TabIndex = 0
        '
        'txtUsuario1
        '
        Me.txtUsuario1.Location = New System.Drawing.Point(39, 19)
        Me.txtUsuario1.Name = "txtUsuario1"
        Me.txtUsuario1.Size = New System.Drawing.Size(270, 20)
        Me.txtUsuario1.TabIndex = 2
        '
        'btnEnviarUsuario1
        '
        Me.btnEnviarUsuario1.Location = New System.Drawing.Point(39, 55)
        Me.btnEnviarUsuario1.Name = "btnEnviarUsuario1"
        Me.btnEnviarUsuario1.Size = New System.Drawing.Size(75, 23)
        Me.btnEnviarUsuario1.TabIndex = 3
        Me.btnEnviarUsuario1.Text = "Enviar"
        Me.btnEnviarUsuario1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtUsuario1)
        Me.GroupBox1.Controls.Add(Me.btnEnviarUsuario1)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(329, 100)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Usuario1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtUsuario2)
        Me.GroupBox2.Controls.Add(Me.btnEnviarUsuario2)
        Me.GroupBox2.Location = New System.Drawing.Point(378, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(283, 100)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Usuario2"
        '
        'txtUsuario2
        '
        Me.txtUsuario2.Location = New System.Drawing.Point(39, 19)
        Me.txtUsuario2.Name = "txtUsuario2"
        Me.txtUsuario2.Size = New System.Drawing.Size(225, 20)
        Me.txtUsuario2.TabIndex = 2
        '
        'btnEnviarUsuario2
        '
        Me.btnEnviarUsuario2.Location = New System.Drawing.Point(39, 55)
        Me.btnEnviarUsuario2.Name = "btnEnviarUsuario2"
        Me.btnEnviarUsuario2.Size = New System.Drawing.Size(75, 23)
        Me.btnEnviarUsuario2.TabIndex = 3
        Me.btnEnviarUsuario2.Text = "Enviar"
        Me.btnEnviarUsuario2.UseVisualStyleBackColor = True
        '
        'frmChat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(673, 262)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "frmChat"
        Me.Text = "Chat"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents txtUsuario1 As System.Windows.Forms.TextBox
    Friend WithEvents btnEnviarUsuario1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtUsuario2 As System.Windows.Forms.TextBox
    Friend WithEvents btnEnviarUsuario2 As System.Windows.Forms.Button

End Class
