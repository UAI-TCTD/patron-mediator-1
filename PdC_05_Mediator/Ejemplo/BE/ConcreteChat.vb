﻿Public Class ConcreteChat
    Inherits Chat

    Private _participantes As Dictionary(Of String, Usuarios) = New Dictionary(Of String, Usuarios)

    Public Overrides Sub Enviar(_mensaje As String, _para As Usuarios, _desde As Usuarios)
        _participantes(_para.Nombre).Recibir(_mensaje, _desde)
    End Sub

    Public Overrides Sub Register(_usuario As Usuarios)


        _participantes.Add(_usuario.Nombre, _usuario)

    End Sub
End Class
