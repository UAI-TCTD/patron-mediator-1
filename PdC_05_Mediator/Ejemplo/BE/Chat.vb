﻿Public MustInherit Class Chat
    Public MustOverride Sub Register(_usuario As Usuarios)
    Public MustOverride Sub Enviar(_mensaje As String, _para As Usuarios, _desde As Usuarios)

End Class
