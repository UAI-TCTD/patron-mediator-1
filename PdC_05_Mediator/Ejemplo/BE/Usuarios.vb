﻿Public MustInherit Class Usuarios
    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return Me.Nombre
    End Function

    Public MustOverride Sub Recibir(_mensaje As String, _de As Usuarios)
    Public MustOverride Sub Enviar(_mensaje As String, _para As Usuarios)
    Private _chat As Chat
    Public Property Chat() As Chat
        Get
            Return _chat
        End Get
        Set(ByVal value As Chat)
            _chat = value
        End Set
    End Property


End Class
